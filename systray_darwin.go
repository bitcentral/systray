//go:build darwin
// +build darwin

package systray

/*
#cgo darwin CFLAGS: -DDARWIN -x objective-c -fobjc-arc -Wformat-security
#cgo darwin LDFLAGS: -framework Cocoa -framework WebKit
#include <stdlib.h>
#include "systray.h"
*/
import "C"

import (
	"fmt"
	"strings"
	"unsafe"
)

type startCallback func()
type urlCallback func(url string)

var (
	startupCallbacks = []startCallback{}
	urlCallbacks     = []urlCallback{}
)

const (
	// NSAlertFirstButtonReturn return value when first button in a prompt is clicked
	NSAlertFirstButtonReturn = 1000
	// NSAlertSecondButtonReturn return value when second buttion in a prompt is clicked
	NSAlertSecondButtonReturn = 1001
	// NSAlertThirdButtonReturn return value when third button in a prompt is clicked
	NSAlertThirdButtonReturn = 1002
)

// SetTemplateIcon sets the systray icon as a template icon (on Mac), falling back
// to a regular icon on other platforms.
// templateIconBytes and regularIconBytes should be the content of .ico for windows and
// .ico/.jpg/.png for other platforms.
func SetTemplateIcon(templateIconBytes []byte, regularIconBytes []byte) {
	cstr := (*C.char)(unsafe.Pointer(&templateIconBytes[0]))
	C.setIcon(cstr, (C.int)(len(templateIconBytes)), true)
}

// SetIcon sets the icon of a menu item. Only works on macOS and Windows.
// iconBytes should be the content of .ico/.jpg/.png
func (item *MenuItem) SetIcon(iconBytes []byte) {
	cstr := (*C.char)(unsafe.Pointer(&iconBytes[0]))
	C.setMenuItemIcon(cstr, (C.int)(len(iconBytes)), C.int(item.id), false)
}

// SetTemplateIcon sets the icon of a menu item as a template icon (on macOS). On Windows, it
// falls back to the regular icon bytes and on Linux it does nothing.
// templateIconBytes and regularIconBytes should be the content of .ico for windows and
// .ico/.jpg/.png for other platforms.
func (item *MenuItem) SetTemplateIcon(templateIconBytes []byte, regularIconBytes []byte) {
	cstr := (*C.char)(unsafe.Pointer(&templateIconBytes[0]))
	C.setMenuItemIcon(cstr, (C.int)(len(templateIconBytes)), C.int(item.id), true)
}

// cocoa added here - start

// AutoStart sets whether app starts automatically at login.
func AutoStart(flag bool) {
	C.autoStart(C.bool(flag))
}

// BundleIdentifier returns this app's bundle identifier in reverse RFC 1034 (e.g. com.bitbucket.djlawhead)
func BundleIdentifier() string {
	return C.GoString(C.bundlePath())
}

// BundlePath path of bundle on filesystem
func BundlePath() string {
	return C.GoString(C.bundleIdentifier())
}

// AddUrlCallback adds a callback which will receive the URL app was started with.
func AddUrlCallback(c urlCallback) {
	urlCallbacks = append(urlCallbacks, c)
}

// AddStartCallback adds callback function which is called when app starts.
func AddStartCallback(c startCallback) {
	startupCallbacks = append(startupCallbacks, c)
}

//TODO: refactor name after successful runs
//export cocoaStart
func cocoaStart() {
	fmt.Println("systray Start")
	for _, f := range startupCallbacks {
		f()
	}
}

//export cocoaUrl
func cocoaUrl(data *C.char) {
	fmt.Println("systray cocoaUrl")
	url := C.GoString(data)
	for _, f := range urlCallbacks {
		f(url)
	}
}

func Start() {
	// C.registerSystray()
}

func Log(message string) {
	msgStr := C.CString(message)
	C.printLog(msgStr)
	C.free(unsafe.Pointer(msgStr))
}

// ShowFileDialog shows a file-system dialog
func ShowFileDialog(title, defaultDirectory string,
	fileTypes []string,
	forFiles bool, multiselect bool) []string {

	titlePtr := C.CString(title)
	dirPtr := C.CString(defaultDirectory)
	filesCsv := C.CString(strings.Join(fileTypes, ","))

	filesBool := C.bool(false)
	if forFiles {
		filesBool = C.bool(true)
	}

	selectBool := C.bool(false)
	if multiselect {
		selectBool = C.bool(true)
	}

	result := C.cocoaFSDialog(titlePtr, filesCsv, dirPtr, filesBool, selectBool)

	defer func() {
		C.free(unsafe.Pointer(titlePtr))
		C.free(unsafe.Pointer(dirPtr))
		C.free(unsafe.Pointer(filesCsv))
	}()

	retval := C.GoString(result)
	return strings.Split(retval, "\n")
}

// ShowDialog shows a dialog/alert message
func ShowDialog(message string) {
	msgStr := C.CString(message)
	C.cocoaDialog(msgStr)
	C.free(unsafe.Pointer(msgStr))
}

// ShowPrompt shows a yes/no dialog prompt
func ShowPrompt(message, buttonLabel, altButtonLabel string) int {
	msgStr := C.CString(message)
	btn1 := C.CString(buttonLabel)
	btn2 := C.CString(altButtonLabel)

	defer func() {
		C.free(unsafe.Pointer(msgStr))
		C.free(unsafe.Pointer(btn1))
		C.free(unsafe.Pointer(btn2))
	}()

	retval := int(C.cocoaPrompt(msgStr, btn1, btn2))

	return retval
}

// cocoa added here - end
