#import <Cocoa/Cocoa.h>
#include "systray.h"

extern void cocoaStart();
extern void cocoaUrl(char *url);

#if __MAC_OS_X_VERSION_MIN_REQUIRED < 101400

    #ifndef NSControlStateValueOff
      #define NSControlStateValueOff NSOffState
    #endif

    #ifndef NSControlStateValueOn
      #define NSControlStateValueOn NSOnState
    #endif

#endif

@interface MenuItem : NSObject
{
  @public
    NSNumber* menuId;
    NSNumber* parentMenuId;
    NSString* title;
    NSString* tooltip;
    short disabled;
    short checked;
}
-(id) initWithId: (int)theMenuId
withParentMenuId: (int)theParentMenuId
       withTitle: (const char*)theTitle
     withTooltip: (const char*)theTooltip
    withDisabled: (short)theDisabled
     withChecked: (short)theChecked;
     @end
     @implementation MenuItem
     -(id) initWithId: (int)theMenuId
     withParentMenuId: (int)theParentMenuId
            withTitle: (const char*)theTitle
          withTooltip: (const char*)theTooltip
         withDisabled: (short)theDisabled
          withChecked: (short)theChecked
{
  menuId = [NSNumber numberWithInt:theMenuId];
  parentMenuId = [NSNumber numberWithInt:theParentMenuId];
  title = [[NSString alloc] initWithCString:theTitle
                                   encoding:NSUTF8StringEncoding];
  tooltip = [[NSString alloc] initWithCString:theTooltip
                                     encoding:NSUTF8StringEncoding];
  disabled = theDisabled;
  checked = theChecked;
  return self;
}
@end

@interface AppDelegate: NSObject <NSApplicationDelegate>
  - (void) add_or_update_menu_item:(MenuItem*) item;
  - (IBAction)menuHandler:(id)sender;
  - (void)handleGetURLEvent:(NSAppleEventDescriptor *)event withReplyEvent:(NSAppleEventDescriptor *)replyEvent;
  - (NSString *)bundleIdentifier;
  - (NSString *)bundlePath;

  - (void)showDialogWithMessage:(NSString *)message;
  - (void)showDialogWithMessage:(NSString *)message
             andButtonLeftLabel:(NSString *)button1
               rightButtonLabel:(NSString *)button2
              completionHandler:(void (^)(NSModalResponse returnCode))handler;

  - (void)showFilesystemDialogWithTitle:(NSString *)title 
                              fileTypes:(NSArray *)fileTypes
                            initialPath:(NSURL *)initialPath
                      enableMultiSelect:(BOOL)multiSelection
                            selectFiles:(BOOL)selectFiles
                      completionHandler:(void (^)(NSString *csv))handler;

  @property (nonatomic,assign) BOOL autoLaunch;
  @property (assign) IBOutlet NSWindow *window;

@end

@implementation AppDelegate
{
  NSStatusItem *statusItem;
  NSMenu *menu;
  NSCondition* cond;
}

@synthesize window = _window;

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
  self->statusItem = [[NSStatusBar systemStatusBar] statusItemWithLength:NSVariableStatusItemLength];
  self->menu = [[NSMenu alloc] init];
  [self->menu setAutoenablesItems: FALSE];
  [self->statusItem setMenu:self->menu];

  systray_ready();

  cocoaStart();
}

- (void)applicationWillTerminate:(NSNotification *)aNotification
{
  systray_on_exit();
}

- (void)setIcon:(NSImage *)image {
  statusItem.button.image = image;
  [self updateTitleButtonStyle];
}

- (void)setTitle:(NSString *)title {
  statusItem.button.title = title;
  [self updateTitleButtonStyle];
}

-(void)updateTitleButtonStyle {
  if (statusItem.button.image != nil) {
    if ([statusItem.button.title length] == 0) {
      statusItem.button.imagePosition = NSImageOnly;
    } else {
      statusItem.button.imagePosition = NSImageLeft;
    }
  } else {
    statusItem.button.imagePosition = NSNoImage;
  }
}

- (void)setTooltip:(NSString *)tooltip {
  statusItem.button.toolTip = tooltip;
}

- (IBAction)menuHandler:(id)sender
{
  NSNumber* menuId = [sender representedObject];
  systray_menu_item_selected(menuId.intValue);
}

- (void)add_or_update_menu_item:(MenuItem *)item {
  NSMenu *theMenu = self->menu;
  NSMenuItem *parentItem;
  if ([item->parentMenuId integerValue] > 0) {
    parentItem = find_menu_item(menu, item->parentMenuId);
    if (parentItem.hasSubmenu) {
      theMenu = parentItem.submenu;
    } else {
      theMenu = [[NSMenu alloc] init];
      [theMenu setAutoenablesItems:NO];
      [parentItem setSubmenu:theMenu];
    }
  }
  
  NSMenuItem *menuItem;
  menuItem = find_menu_item(theMenu, item->menuId);
  if (menuItem == NULL) {
    menuItem = [theMenu addItemWithTitle:item->title
                               action:@selector(menuHandler:)
                        keyEquivalent:@""];
    [menuItem setRepresentedObject:item->menuId];
  }
  [menuItem setTitle:item->title];
  [menuItem setTag:[item->menuId integerValue]];
  [menuItem setTarget:self];
  [menuItem setToolTip:item->tooltip];
  if (item->disabled == 1) {
    menuItem.enabled = FALSE;
  } else {
    menuItem.enabled = TRUE;
  }
  if (item->checked == 1) {
    menuItem.state = NSControlStateValueOn;
  } else {
    menuItem.state = NSControlStateValueOff;
  }
}

NSMenuItem *find_menu_item(NSMenu *ourMenu, NSNumber *menuId) {
  NSMenuItem *foundItem = [ourMenu itemWithTag:[menuId integerValue]];
  if (foundItem != NULL) {
    return foundItem;
  }
  NSArray *menu_items = ourMenu.itemArray;
  int i;
  for (i = 0; i < [menu_items count]; i++) {
    NSMenuItem *i_item = [menu_items objectAtIndex:i];
    if (i_item.hasSubmenu) {
      foundItem = find_menu_item(i_item.submenu, menuId);
      if (foundItem != NULL) {
        return foundItem;
      }
    }
  }

  return NULL;
};

- (void) add_separator:(NSNumber*) menuId
{
  [menu addItem: [NSMenuItem separatorItem]];
}

- (void) hide_menu_item:(NSNumber*) menuId
{
  NSMenuItem* menuItem = find_menu_item(menu, menuId);
  if (menuItem != NULL) {
    [menuItem setHidden:TRUE];
  }
}

- (void) setMenuItemIcon:(NSArray*)imageAndMenuId {
  NSImage* image = [imageAndMenuId objectAtIndex:0];
  NSNumber* menuId = [imageAndMenuId objectAtIndex:1];

  NSMenuItem* menuItem;
  menuItem = find_menu_item(menu, menuId);
  if (menuItem == NULL) {
    return;
  }
  menuItem.image = image;
}

- (void) show_menu_item:(NSNumber*) menuId
{
  NSMenuItem* menuItem = find_menu_item(menu, menuId);
  if (menuItem != NULL) {
    [menuItem setHidden:FALSE];
  }
}

- (void) quit
{
  [NSApp terminate:self];
}

// cocoa moved here - start
- (void)applicationWillFinishLaunching:(NSNotification *)aNotification {
    NSAppleEventManager *appleEventManager = [NSAppleEventManager sharedAppleEventManager];
    [appleEventManager setEventHandler:self
                       andSelector:@selector(handleGetURLEvent:withReplyEvent:)
                       forEventClass:kInternetEventClass andEventID:kAEGetURL];
}

- (void)applicationDidEnterBackground:(NSApplication *)app {}

- (void)applicationDidBecomeActive:(NSApplication *)app {}

- (void)handleGetURLEvent:(NSAppleEventDescriptor *)event withReplyEvent:(NSAppleEventDescriptor *)replyEvent
{
    NSURL *url = [NSURL URLWithString:[[event paramDescriptorForKeyword:keyDirectObject] stringValue]];
    cocoaUrl((char *)[[url absoluteString] UTF8String]);
}

- (void)showDialogWithMessage:(NSString *)message {
    NSAlert *alert = [[NSAlert alloc] init];
    [alert addButtonWithTitle:@"OK"];
    [alert setInformativeText:message];
    [alert setAlertStyle:0];
    [alert runModal];
}

- (void) showDialogWithMessage:(NSString *)message
            andButtonLeftLabel:(NSString *)button1
              rightButtonLabel:(NSString *)button2
             completionHandler:(void (^)(NSModalResponse returnCode))handler {
    NSAlert *alert = [[NSAlert alloc] init];
    [alert addButtonWithTitle:button1];
    [alert addButtonWithTitle:button2];
    [alert setInformativeText:message];
    [alert setAlertStyle:0];
    handler([alert runModal]);
}

- (void)showFilesystemDialogWithTitle:(NSString *)title 
                                    fileTypes:(NSArray *)fileTypes
                                  initialPath:(NSURL *)initialPath
                            enableMultiSelect:(BOOL)multiSelection
                                  selectFiles:(BOOL)selectFiles
                            completionHandler:(void (^)(NSString *csv))handler {
    NSOpenPanel *openPanel = [NSOpenPanel openPanel];
    [openPanel setFloatingPanel:YES];
    [openPanel setCanChooseFiles:selectFiles];
    [openPanel setCanChooseDirectories:!selectFiles];
    [openPanel setAllowsMultipleSelection:multiSelection];
    [openPanel setDirectoryURL:initialPath];
    [openPanel setTitle:title];
    if ([fileTypes count] > 0)
        [openPanel setAllowedFileTypes:fileTypes];
    
    [openPanel beginWithCompletionHandler:^(NSInteger result){
        if (result  == NSModalResponseOK)
        {
            NSMutableArray *selectedPaths = [[NSMutableArray alloc] init];
            for (NSURL *url in [openPanel URLs])
            {
                [selectedPaths addObject:[url path]];
            }
            handler([selectedPaths componentsJoinedByString:@"\n"]);
        }
        else {
            handler(nil);
        }
    }];  
}

// TODO forward applescript errors up to the go layer for proper error handling
- (void)setAutoLaunch:(BOOL)flag forApplication:(NSString *)appName atPath:(NSString *)path
{
    static const AEKeyword aeName = 'pnam';
    static const AEKeyword aePath = 'ppth';
    NSString *src = @"tell application \"System Events\" to get the name of every login item";
    BOOL alreadyAdded = false;
    NSAppleScript *script = [[NSAppleScript alloc] initWithSource:src];
    NSDictionary *err = nil;
    NSAppleEventDescriptor *evtDesc = [script executeAndReturnError:&err];
    script = nil;
    for (int i = 0; i < [evtDesc numberOfItems]; ++i)
    {
        NSAppleEventDescriptor *loginItem = [evtDesc descriptorAtIndex:i];
        NSString *loginItemName = [[loginItem descriptorForKeyword:aeName] stringValue];
        if ([loginItemName isEqualTo:appName])
        {
            alreadyAdded = TRUE;
        }
    }
    evtDesc = nil;
    if (flag && !alreadyAdded)
    {
        src = @"tell application \"System Events\" to make login item at end with properties {name:\"%s\",path:\"%s\",hidden:false}";
    }
    else if (!flag && alreadyAdded)
    {
        src = @"tell application \"System Events\" to delete login item \"%s\"";
    }
    script = [[NSAppleScript alloc] initWithSource:[NSString stringWithFormat:src, [appName UTF8String], [path UTF8String]]];
    evtDesc = [script executeAndReturnError:&err]; 
}

- (NSString *)bundleIdentifier {
    if ([NSBundle mainBundle] == nil) return @"com.apple.Safari";
    return [[NSBundle mainBundle] bundleIdentifier];
}

- (NSString *)bundlePath {
    if ([NSBundle mainBundle] == nil) return @"/Applications/Safari.app/Contents/MacOS/Safari";
    return [[NSBundle mainBundle] bundlePath];
}
@end

AppDelegate *getDelegate() {
    return (AppDelegate *)[NSApp delegate];
}

char* bundlePath() {
    __block char *retval = nil;
    AppDelegate *delegate = getDelegate();
    dispatch_sync(dispatch_get_main_queue(), ^{
        retval = (char *)[[delegate bundlePath] UTF8String];
    });
    return retval;
}

char* bundleIdentifier() {
    __block char *retval = nil;
    AppDelegate *delegate = getDelegate();
    dispatch_sync(dispatch_get_main_queue(), ^{
        NSString *identifier = [delegate bundleIdentifier];
        if (identifier == nil) identifier = @"unknown.bundle";
        retval =  (char *)[identifier UTF8String];
    });
    return retval;
}

void autoStart(bool flag) {
    AppDelegate *delegate = (AppDelegate *)[NSApp delegate];
    [delegate setAutoLaunch:flag forApplication:[delegate bundleIdentifier] atPath:[delegate bundlePath]];
}

void runOnMainQueueWithoutDeadlocking(void (^ block)(dispatch_semaphore_t s))
{
    if ([NSThread isMainThread])
    {
        block(nil);
    }
    else
    {
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        dispatch_async(dispatch_get_main_queue(), ^{
            block(sema);
        });
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
        sema = nil;
    }
}


void cocoaDialog(char *msg) {
    dispatch_async(dispatch_get_main_queue(), ^{
        [(AppDelegate *)[NSApp delegate] showDialogWithMessage:[NSString stringWithUTF8String:msg]];
    });
}

int cocoaPrompt(char *msg, char *btn1, char *btn2) {
    __block NSUInteger retval = 0;
    dispatch_semaphore_t sem = dispatch_semaphore_create(0);
    void (^handler)(NSModalResponse) = ^(NSModalResponse response){
        retval = (NSUInteger)response;
        dispatch_semaphore_signal(sem);
    };
    dispatch_async(dispatch_get_main_queue(), ^{
        [(AppDelegate *)[NSApp delegate] showDialogWithMessage:[NSString stringWithUTF8String:msg]
                                                 andButtonLeftLabel:[NSString stringWithUTF8String:btn1]
                                                   rightButtonLabel:[NSString stringWithUTF8String:btn2]
                                                  completionHandler:handler]; 
    });
    [NSApp activateIgnoringOtherApps:YES];
    dispatch_semaphore_wait(sem, DISPATCH_TIME_FOREVER);
    sem = nil;
    return (int)retval;
}

const char* cocoaFSDialog(char *title,
    char *fileTypesCsv,
    char *initialPath,
    bool canChooseFiles,
    bool multiSelection) {

    sleep(1);

    NSURL *initialURL = nil;
    NSString *titleStr = nil;
    NSArray *fileTypesArr = nil;

    if (initialPath != NULL) {
        initialURL = [NSURL fileURLWithPath:[NSString stringWithCString:initialPath encoding:NSUTF8StringEncoding]];
    }
    if (title != NULL) {
        titleStr = [[NSString alloc] initWithUTF8String:title];
    }
    if (fileTypesCsv != NULL)  {
        NSString *csvStr = [[NSString alloc] initWithUTF8String:fileTypesCsv];
    if (![csvStr isEqualTo:@""])
            fileTypesArr = [csvStr componentsSeparatedByString:@","];
    }
    
    __block NSString *blockret = NULL;
    dispatch_semaphore_t sem = dispatch_semaphore_create(0);

    dispatch_async(dispatch_get_main_queue(), ^{
        AppDelegate *delegate = (AppDelegate *)[NSApp delegate];
        [delegate showFilesystemDialogWithTitle:titleStr
            fileTypes:fileTypesArr
            initialPath:initialURL
            enableMultiSelect:multiSelection
            selectFiles:canChooseFiles
            completionHandler:^(NSString *csv) {
                if (csv != nil) { 
                    blockret = csv;
                }
                dispatch_semaphore_signal(sem);
            }
        ];
    });
    [NSApp activateIgnoringOtherApps:YES];
    dispatch_semaphore_wait(sem, DISPATCH_TIME_FOREVER);
    sem = nil;
        
    if (blockret != nil) {
        char *retval = (char *)[[blockret copy] UTF8String];
        return retval;
    }
    return nil;
}
// cocoa moved here - end

void registerSystray(void) {
  AppDelegate *delegate = [[AppDelegate alloc] init];
  [[NSApplication sharedApplication] setDelegate:delegate];
  // A workaround to avoid crashing on macOS versions before Catalina. Somehow
  // SIGSEGV would happen inside AppKit if [NSApp run] is called from a
  // different function, even if that function is called right after this.
  if (floor(NSAppKitVersionNumber) <= /*NSAppKitVersionNumber10_14*/ 1671){
    [NSApp run];
  }
}

int nativeLoop(void) {
  if (floor(NSAppKitVersionNumber) > /*NSAppKitVersionNumber10_14*/ 1671){
    [NSApp run];
  }
  return EXIT_SUCCESS;
}

void runInMainThread(SEL method, id object) {
  [(AppDelegate*)[NSApp delegate]
    performSelectorOnMainThread:method
                     withObject:object
                  waitUntilDone: YES];
}

void setIcon(const char* iconBytes, int length, bool template) {
  NSData* buffer = [NSData dataWithBytes: iconBytes length:length];
  NSImage *image = [[NSImage alloc] initWithData:buffer];
  [image setSize:NSMakeSize(16, 16)];
  image.template = template;
  runInMainThread(@selector(setIcon:), (id)image);
}

void setMenuItemIcon(const char* iconBytes, int length, int menuId, bool template) {
  NSData* buffer = [NSData dataWithBytes: iconBytes length:length];
  NSImage *image = [[NSImage alloc] initWithData:buffer];
  [image setSize:NSMakeSize(16, 16)];
  image.template = template;
  NSNumber *mId = [NSNumber numberWithInt:menuId];
  runInMainThread(@selector(setMenuItemIcon:), @[image, (id)mId]);
}

void setTitle(char* ctitle) {
  NSString* title = [[NSString alloc] initWithCString:ctitle
                                             encoding:NSUTF8StringEncoding];
  free(ctitle);
  runInMainThread(@selector(setTitle:), (id)title);
}

void setTooltip(char* ctooltip) {
  NSString* tooltip = [[NSString alloc] initWithCString:ctooltip
                                               encoding:NSUTF8StringEncoding];
  free(ctooltip);
  runInMainThread(@selector(setTooltip:), (id)tooltip);
}

void add_or_update_menu_item(int menuId, int parentMenuId, char* title, char* tooltip, short disabled, short checked, short isCheckable) {
  MenuItem* item = [[MenuItem alloc] initWithId: menuId withParentMenuId: parentMenuId withTitle: title withTooltip: tooltip withDisabled: disabled withChecked: checked];
  free(title);
  free(tooltip);
  runInMainThread(@selector(add_or_update_menu_item:), (id)item);
}

void add_separator(int menuId) {
  NSNumber *mId = [NSNumber numberWithInt:menuId];
  runInMainThread(@selector(add_separator:), (id)mId);
}

void hide_menu_item(int menuId) {
  NSNumber *mId = [NSNumber numberWithInt:menuId];
  runInMainThread(@selector(hide_menu_item:), (id)mId);
}

void show_menu_item(int menuId) {
  NSNumber *mId = [NSNumber numberWithInt:menuId];
  runInMainThread(@selector(show_menu_item:), (id)mId);
}

void printLog(char *msg) {
    NSString *message = [NSString stringWithUTF8String:msg];
}

void quit() {
  runInMainThread(@selector(quit), nil);
}